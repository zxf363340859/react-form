import React, {Component} from 'react';
import './myProfile.less';

class MyProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            gender: 'male',
            checkbos: false,
            desc: ''
        }
        this.handleName = this.handleName.bind(this);
        this.handleGender = this.handleGender.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
    }

    handleName(event) {
        this.setState({
            name: event.target.value
        });
    }

    handleGender(event) {
        this.setState({
            gender: event.target.value
        });
    }

    handleDescription(event) {
        this.setState({
            desc: event.target.value
        });
    }

    handleCheckbox() {
        this.state.checkbox ?
            this.setState({
                checkbos: false
            }) :
            this.setState({
            checkbos: true
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.name, this.state.gender, this.state.checkbos,this.state.desc)
    }


  render() {
    return (
      <form className={'container'}>
        <h1>My Profiles</h1>
        Name<br/><input type = {'text'} onChange={this.handleName} value={this.state.name} /> <br/>
          Gender<br/><select onChange={this.handleGender} value={this.state.gender}>
                    <option>male</option>
                    <option>famale</option>
                </select><br/>
        Description<br/><input type = {'textarea'} onChange={this.handleDescription} value={this.state.desc} /><br/>
        <input type={'checkbox'}   onChange={this.handleCheckbox} value={this.state.checkbox} /> I have read the terms of conduct<br/>
        <input type= 'submit' value={'Submit'} onClick={this.handleSubmit}/>
      </form>
    );
  }
}

export default MyProfile;


